import { useState } from "react";
import "./style.scss";
import Button from "../../components/Button";
import { useHistory } from "react-router-dom";
import logoWhite from "../../assets/img/brand-vertical-white.png";
import LoginService from "../../services/requests/login";

import { useSelector } from "react-redux";

const Confirmacao = () => {
  const history = useHistory();
  const [emailResended, setEmailResended] = useState(false);
  const dataUser = useSelector((state: any) => state.user.data)

  const onClickLogin = () => {
    history.replace("/login");
  }

  const onClickReenviarEmail = () => {
    LoginService.reenviarEmailConfirmar(dataUser.email)
     .then(() => {
        setEmailResended(true);
        var timerMinutes = 1;
        var timerSeconds = 59;
        var smsInterval = setInterval(() => {
          if(timerMinutes ===0 && timerSeconds === 0){
            setEmailResended(false);
            clearInterval(smsInterval);
            return;
          }
          timerSeconds--;
        }, 1000);
      });
  }

  // const onClickResetPAge = () => {
  //   setEmailResended(false);
  // }

  return (
      <div className="main-confirmation">
        <div className='confirmation-main'>        
          <div className='content-confirmation'>
            <img className='logo-desktop' src={logoWhite} alt="Logo" />
            <label className="container-confirmation-text" style={{marginTop: 20}}>Você precisa confirmar o seu</label>
            <label className="container-confirmation-text">e-mail para concluir a</label>
            <label className="container-confirmation-text">criação da sua conta</label>

            <label className="container-confirmation-secundary-text">Acesse o seu email e siga os passos por lá.</label>
            
            {emailResended ?
              <div style={{marginTop:20}}>
                <Button className="white-transparent sm button-confirmation disablesButton" >E-mail reenviado!</Button>
              </div>
              :   
              <div style={{marginTop:20}}>
                <Button className="white-transparent sm button-confirmation" onClick={onClickReenviarEmail}>Não recebeu o e-mail? Reenviar</Button>
              </div>
            }

            <div style={{marginTop:20}}>
              <Button className="white-transparent sm button-confirmation" onClick={onClickLogin}>Já confirmou seu e-mail? Faça login</Button>
            </div>
          </div>
        
        </div>
      </div>
    
  );
};
export default Confirmacao;
