import "./style.scss";
import logoWhite from "../../assets/img/brand-vertical-white.png";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import Button from "../../components/Button";

const NovaSenhaEnviada = () => {
  const history = useHistory();
  const dataUser = useSelector((state: any) => state.user.data)

  const onClickLogin = () => {
    history.replace("/login");
  }

  return (
    <div className="content-fill-center">
      <div className="content-centered-slim">
        <img className="mb-8" src={logoWhite} alt="Logo" />

        <h2 className="text-center text-white mb-5 heading-05">
          O e-mail para criar a nova senha foi enviado
        </h2>
        <p className="text-center text-white">
          Enviamos um e-mail de redefinição de senha para {dataUser.email} com
          instruções sobre como redefinir sua senha.
        </p>
        <p className="text-center text-white mb-10">
          Se você não receber o e-mail, verifique se o endereço de e-mail
          inserido está correto, consulte nossas{" "}
          <a className="text-white text-bold" href="#">Perguntas frequentes</a> ou entre em contato com o suporte{" "}
          <a className="text-white text-bold" href="#">aqui</a>.
        </p>

        <div className="action-btn">
        <div style={{marginTop:20}}>
              <Button className="white-transparent sm button-confirmation" onClick={onClickLogin}>Voltar para login</Button>
            </div>
        </div>
      </div>
    </div>
  );
};
export default NovaSenhaEnviada;
