import "./style.scss";
import Button from "../../components/Button";
import { useHistory } from "react-router-dom";
import logoWhite from "../../assets/img/brand-vertical-white.png";
import { useEffect, useState } from "react";
import {useLocation} from "react-router-dom";
import LoginService from "../../services/requests/login";
const ConfirmacaoSucesso = () => {
  const history = useHistory();
  const search = useLocation().search;
  const emailParam = new URLSearchParams(search).get('email');
  const tokenParam = new URLSearchParams(search).get('token');
  const [validado, setValidado]= useState(false);

  const onClickLogin = () => {
    history.replace("/login");
  }

  useEffect(() =>{
    LoginService.confirmarEmail({
      email: emailParam?.toString(), 
      token: tokenParam?.replaceAll(' ', '+') 
    }).then((response) =>{
      if((response?.data?.status === 400)){
        history.push(`/confirmacao`);
      }
      setValidado(true);
    });
  }, [])

  return (
    validado === true ?
      <div className="main-confirmation">
        <div className='confirmation-main'>        
          <div className='content-confirmation'>
            <img className='logo-desktop' src={logoWhite} alt="Logo" />
            <label className="container-confirmation-text" style={{marginTop: 20}}>Seu e-mail de cadastro foi</label>
            <label className="container-confirmation-text">confirmado com sucesso!</label>

            <label className="container-confirmation-secundary-text">Acesse agora mesmo o Portal ESG com segurança.</label>
           
            <div style={{marginTop:20}}>
              <Button className="white-transparent sm button-confirmation-success" onClick={onClickLogin}>Acesse sua conta</Button>
            </div>
          </div>
        
        </div>
      </div>
    :
    <div></div>
  );
};
export default ConfirmacaoSucesso;
