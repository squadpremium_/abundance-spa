import React from "react";
import "./style.scss";
import Sidebar from "../../components/Sidebar";
import FormCadastro from "../../containers/FormCadastro";
import Footer from "../../components/Footer";
import InfoCriarConta from "../../components/InfoCriarConta";
import FormCadastroPJ from "../../containers/FormCadastroPJ";
import InfoJaTemConta from "../../components/InfoJaTemConta";

const CadastroPJ = () => {
  return(
      <div className='container-sidebar'>
          <Sidebar/>
          <div className='content-side'>
            <InfoJaTemConta/>
            <FormCadastroPJ/>
            <Footer/>
          </div>
      </div>
  );
}
export default CadastroPJ
