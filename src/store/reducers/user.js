const initialState = {
  loading: false,
  data: {
    id: "",
    name: "",
    email: "",
    phoneNumber: "",
    shortName: ""
  },
  token: ""
};

export default function user(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case "USER_LOGIN_LOADING":
      return {
        ...state,
        loading: true,
      }

    case "USER_LOGIN_SUCCESS":
      localStorage.setItem("token", payload.token)
      return {
        ...state,
        loading: false,
        data: payload.user,
        token: payload.token
        
      }

    case "USER_LOGIN_REJECTED":
      return initialState;
  
    default:
      return state;
  }
};