import { combineReducers } from "redux";

import user from "./user";
import menu from "./menu";
import search from "./search";
import loader from "./loader";

const reducers = {
  user,
  menu,
  search,
  loader,
};

export default combineReducers(reducers);
