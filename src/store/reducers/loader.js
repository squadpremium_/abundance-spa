const initialState = {
  loading: false,
};

export default function setLoader(state = initialState, action) {
  const { type } = action;
  switch (type) {
    case "LOADING_ACTIVE":
      return {
        ...state,
        loading: true,
      };

    case "LOADING_INACTIVE":
      return {
        ...state,
        loading: false,
      };

    default:
      return {
        ...state,
        loading: false,
      };
  }
}
