import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as Yup from "yup";

import Button from "../../components/Button";
import Input from "../../components/Input";
import "./style.scss";
import { yupResolver } from "@hookform/resolvers/yup";

import FormLoginData from "../../types/FormLogin.interface";
import LoginService from "../../services/requests/login";
import LinkButton from "../../components/LinkButton";
//import ButtonPassword from "../../components/ButtonPassword";
import InputPassword from "../../components/InputPassword";
import { useDispatch } from "react-redux";

const FormLogin = () => {
  const dispatch = useDispatch();
  const [valueForm, setValueForm] = useState({});
  const history = useHistory();

  const validationSchema = Yup.object().shape({
    email: Yup.string().required("E-mail é obrigatório").email('E-mail inválido'),
    password: Yup.string().required("Senha é obrigatório")
    .min(6, "Senha deve ter entre 6-100 caractéres.")
    .max(100, "Senha deve ter entre 6-100 caractéres."),
  });

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<FormLoginData>({
    mode: "all",
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = (data: FormLoginData) => {
    LoginService.login(data)
      .then((response) => {

        if((response?.data?.statusMessage)){
          var statusMessage = response?.data?.statusMessage;
          var user = {
            email: data.email
          };
  
          dispatch({ type: "USER_LOGIN_SUCCESS",
          payload: {
            user,
          token: ""
          }});
  
          // var urlArray = response?.data?.url.toString().split('/');
           var url = "login";
          //Remover após mapeamento de rotas da API
          if(statusMessage === 'NotAllowed'){
            url = 'confirmacao';
          }
          else if(statusMessage === 'RequiresTwoFactor'){
           url = 'verificacao-login' 
          }
          history.push(`/${url}`);
  
          
        }
      });
  };

  const blurInputForm = (target: HTMLInputElement) => {
    target.classList.add("input-enter");
    if(!target.value || !target.validity.valid || 
      (target.name === 'password' && target.value.length < 6) ||
      // eslint-disable-next-line no-useless-escape
      (target.name === 'email' && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/.test(target.value) === false)){
      target.classList.remove("input-enter");
      target.classList.add("input-error");
    }else{
      target.classList.add("input-valid");
      target.classList.remove("input-enter");
      target.classList.remove("input-error");
    }
    setValueForm({ ...getValues(), [target.name]: target.value });
  };

  const changeForm = (target: HTMLInputElement, erros: any) => {
    setValueForm({ ...getValues(), [target.name]: target.value });
    if(erros !== undefined){
      erros.message = '';
    }
    target.classList.remove("input-error");
    target.classList.add("input-enter");
  };

  const focusInput = (target: HTMLInputElement) => {
    target.classList.add("input-enter");
  };

  return (
    <form className="align-center-form" onSubmit={handleSubmit(onSubmit)}>
      <h2>Acesse sua conta</h2>
      <p>Digite seus detalhes abaixo</p>

      <Input
        tabIndex={0}
        register={register("email", {
          onBlur: (e) => blurInputForm(e.target)
        })}
        error={errors.email?.message}
        name="email"
        type="email"
        label="E-mail"
        placeholder="email@example.com.br"
        onChange={(e) => changeForm(e.target, errors.email)}
        autoComplete="off"
        onFocus={(e:any) => focusInput(e.target)}
      />
      <LinkButton tabIndex={99} to="/esqueci-minha-senha" className="link text-right w-fill p-relative" style={{ marginBottom: "-20px", float: "right", display: "block" }}>Esqueci minha senha</LinkButton>
      <InputPassword
      tabIndex={0}
        register={register("password", {
          onBlur: (e) => blurInputForm(e.target)
        })}
        error={errors.password?.message}
        name="password"
        label="Senha"
        placeholder="••••••••••"
        onChange={(e) => changeForm(e.target, errors.password)}
        autoComplete="off"
        onFocus={(e:any) => focusInput(e.target)}
      />

      <Button
      tabIndex={1}
        type="submit"
        className="md btn btn-login"
        disabled={!validationSchema.isValidSync(valueForm)}
      >
        Entrar
      </Button>
    </form>
  );
};

export default FormLogin;
