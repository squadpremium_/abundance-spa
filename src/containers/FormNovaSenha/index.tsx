import React, { useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import "./style.scss";

import LoginService from "../../services/requests/login";

import Input from "../../components/Input";
import Button from "../../components/Button";
import InputPassword from "../../components/InputPassword";
import FormEsqueceuSenhaData from "../../types/FormEsqueceuSenha.interface";
import { useDispatch } from "react-redux";

const FormNovaSenha = () => {
  const dispatch = useDispatch();
  
  const [valueForm, setValueForm] = useState({});
  const history = useHistory();

  const search = useLocation().search;
  const emailParam = new URLSearchParams(search).get('email');
  const tokenParam = new URLSearchParams(search).get('token');
  const [validado, setValidado]= useState(false);

  const validationSchema = Yup.object().shape({
    password: Yup.string().required("Senha é obrigatório")
    .min(6, "Senha deve ter entre 6-100 caractéres.")
    .max(100, "Senha deve ter entre 6-100 caractéres."),
    passwordConfirm: Yup.string().required("Confirmar Senha é obrigatório").min(6, "Senha deve ter entre 6-100 caractéres.")
    .max(100, "Senha deve ter entre 6-100 caractéres."),
  });

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<FormEsqueceuSenhaData>({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = (data: FormEsqueceuSenhaData) => {
    const resetSenhaModel = {
      email: emailParam?.toString(), 
      token: tokenParam?.replaceAll(' ', '+'),
      password: data.password,
      passwordConfirm: data.passwordConfirm
    }
    LoginService.resetSenha(resetSenhaModel)
      .then((reponse) => {
        if(reponse.status == 204){
          var user = {
            email: data.email
          };

          dispatch({ type: "USER_LOGIN_SUCCESS",
          payload: {
            user,
          token: ""
          }});

          history.replace(`/nova-senha-confirmacao`);
        }
        else{
          history.replace(`/nova-senha-enviada`);
        }
        
      });
  };

  const changeForm = (target: HTMLInputElement) => {
    setValueForm({ ...getValues(), [target.name]: target.value });
  };

  return (
    <form className="align-center-form" onSubmit={handleSubmit(onSubmit)}>
      <h2>Insira a sua nova senha</h2>
      <p>Instruções sobre a criação da nova senha e caracteres</p>

      <InputPassword
        register={register("password")}
        error={errors.password?.message}
        name="password"
        label="Nova Senha"
        placeholder="•••••••••••••••••••••••"
        onChange={(e) => changeForm(e.target)}
      />
      <InputPassword
        register={register("passwordConfirm")}
        error={errors.passwordConfirm?.message}
        name="passwordConfirm"
        label="Confirme a nova Senha"
        placeholder="•••••••••••••••••••••••"
        onChange={(e) => changeForm(e.target)}
      />

      <Button
        type="submit"
        className="md btn btn-enviar"
        disabled={!validationSchema.isValidSync(valueForm)}
      >
        Cadastrar nova senha
      </Button>
    </form>
  );
};

export default FormNovaSenha;
