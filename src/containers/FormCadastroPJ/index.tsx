import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import Button from "../../components/Button";
import Input from "../../components/Input";
import Checkbox from "../../components/Checkbox";
import "./style.scss";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import FormCadastroData from "../../types/FormCadastro.interface";
import CadastroService from "../../services/requests/cadastro";
import Select from "../../components/Select";
import Drawer from "../../components/SliderModal";
import FormTemosECondicoes from "../DrawerInformations/FormTermosECondicoes";
import FormPoliticaDePrivacidade from "../DrawerInformations/FormPoliticaDePrivacidade";
import MaskedInput from "../../components/MaskedInput";
import InputPassword from "../../components/InputPassword";

const FormCadastroPJ = () => {
  const [valueForm, setValueForm] = useState({});
  const [openDrawerTermos, setOpenDrawerTermos] = useState(false);
  const [openDrawerPrivacidade, setOpenDrawerPrivacidade] = useState(false);

  const [empresaValue, setEmpresaValue] = useState(false);
  const history = useHistory();

  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Nome da empresa é obrigatório"),
    email: Yup.string().required("Email é obrigatório").email("E-mail inválido"),
    //nomeEmpresa: Yup.string().required("Nome é obrigatório"),
    password: Yup.string().required("Senha é obrigatório"),
    phoneNumber: Yup.string().required("Telefone é obrigatório"),
    passwordConfirm: Yup.string().required("Confirmar senha é obrigatório"),
    //ondeOuviu: Yup.string().required("Aonde você ouviu falar da Abundance Brasil? é obrigatório"),
  });

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<FormCadastroData>({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = (data: FormCadastroData) => {
    data.isValidPhysicalPerson = true;
    CadastroService.create(data)
      .then(() => {
        return history.replace("/confirmacao");
      });
  };

  const changeForm = (target: HTMLInputElement) => {
    setValueForm({ ...getValues(), [target.name]: target.value });
  };

  const validateEmpresa = () => {
    setEmpresaValue(!empresaValue);
  };

  const handleOpenDrawerTermos = () => {
    setOpenDrawerTermos(true);
  };

  const handleOpenDrawerPrivacidade = () => {
    setOpenDrawerPrivacidade(true);
  };

  useEffect(() => {
    if (openDrawerTermos || openDrawerPrivacidade) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'unset';
    }
  }, [openDrawerTermos, openDrawerPrivacidade]);


  return (
    <form className="align-center-form-pj" onSubmit={handleSubmit(onSubmit)}>
      <label className="label-info">PARA SUA EMPRESA</label>
      <h2>Abra sua conta</h2>
      <p>Digite seus detalhes abaixo para cadastro de Pessoa Jurídica</p>
      <Input
        register={register("name")}
        error={errors.name?.message}
        name="name"
        type="text"
        label="Nome da empresa"
        placeholder="Empresa"
        onChange={(e) => changeForm(e.target)}
        autoComplete="off"
      />
      <Input
        register={register("email")}
        error={errors.email?.message}
        name="email"
        label="E-mail"
        type="email"
        placeholder="meuemail@empresa.com.br"
        onChange={(e) => changeForm(e.target)}
        autoComplete="off"
      />
      <MaskedInput
        register={register("phoneNumber")}
        error={errors.phoneNumber?.message}
        name="phoneNumber"
        type="text"
        label="Telefone"
        placeholder="(XX) XXXXX-XXXX"
        onChange={(e: { target: HTMLInputElement; }) => changeForm(e.target)}
        mask="(99) 99999-9999" maskChar={null}
      />
      {/* <Input
        register={register("nomeEmpresa")}
        error={errors.nomeEmpresa?.message}
        name="nomeEmpresa"
        type="text"
        label="Nome da sua Empresa"
        placeholder="Nome da Empresa"
        onChange={(e) => changeForm(e.target)}
        autoComplete="off"
      /> */}

      <InputPassword
        register={register("password")}
        error={errors.password?.message}
        name="password"
        label="Senha"
        placeholder="•••••••••••••••••••••••"
        onChange={(e) => changeForm(e.target)}
        autoComplete="off"
      />

      <InputPassword
        register={register("passwordConfirm")}
        error={errors.passwordConfirm?.message}
        name="passwordConfirm"
        label="Confirmar Senha"
        placeholder="•••••••••••••••••••••••"
        onChange={(e) => changeForm(e.target)}
        autoComplete="off"
      />

      <Select
        register={register("ondeOuviu")}
        error={errors.ondeOuviu?.message}
        name="ondeOuviu"
        label="Aonde você ouviu falar da Abundance Brasil?"
        onChange={(e) => changeForm(e.target)}
      >
        <option value="" disabled={true} selected={true}>Selecione uma opção</option>
        <option value="google">Google</option>
        <option value="instagram">Instagram</option>
        <option value="linkedin">Linkedin</option>
        <option value="facebook">Facebook</option>
        <option value="youtube">Youtube</option>
        <option value="site">Site</option>
        <option value="indicacao">Indicação</option>

      </Select>

      <Checkbox
        register={register("atualizarInfo")}
        error={errors.atualizarInfo?.message}
        name="atualizarInfo"
        label="Envie-me atualizações, informações do produto e mercado ESG."
      />

      <label className="mb-5 label">
        Ao clicar neste botão, você concorda com nossa <br />
        <div className="button-container">
          <Button type="button" className="link" onClick={handleOpenDrawerPrivacidade} >Política de Privacidade</Button>&#160; e­ &#160;<Button type="button" className="link" onClick={handleOpenDrawerTermos}>Termos e Confições</Button>
        </div>
      </label>

      <Drawer opened={openDrawerPrivacidade} onCLose={() => setOpenDrawerPrivacidade(false)} title="Política de Privacidade">
        <FormPoliticaDePrivacidade />
      </Drawer>

      <Drawer opened={openDrawerTermos} onCLose={() => setOpenDrawerTermos(false)} title="Termos e Condições">
        <FormTemosECondicoes />
      </Drawer>

      <Button
        type="submit"
        className="md btn btn-cadastro"
         disabled={!validationSchema.isValidSync(valueForm)}
      >
        Criar conta
      </Button>
    </form>
  );
};

export default FormCadastroPJ;
