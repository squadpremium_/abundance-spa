import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as Yup from "yup";

import Button from "../../components/Button";
import "./style.scss";
import { yupResolver } from "@hookform/resolvers/yup";

import FormLoginData from "../../types/FormLogin.interface";
import LoginService from "../../services/requests/login";
import MaskedInput from "../../components/MaskedInput";
import { useSelector } from "react-redux";

const VerificacaoLogin = () => {
  const [valueForm, setValueForm] = useState({});
  const history = useHistory();
  const dataUser = useSelector((state: any) => state.user.data)
  
  const [labelBtnSms, setLabelBtnSms] = useState("Reenviar código");
  const [enableBtnSms, setEnableBtnSms] = useState("");

  const validationSchema = Yup.object().shape({
    password: Yup.string().required("Código é obrigatório").test('len', 'Código Inválido', val => val?.length === 6),
  });

  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<FormLoginData>({
    mode: "all",
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = (data: FormLoginData) => {
    var confirmLogin = 
    {
      email: dataUser.email, 
      twoFactorCode: data.password
    };
    LoginService.confirmarLogin(confirmLogin)
      .then(() => {
        return history.push("/home");
      });
  };

  const onResendSms = () =>{
    
    LoginService.reenviarSms(dataUser.email)
     .then(() => {
        setEnableBtnSms("link-confirmacao-login-disabled");
        setLabelBtnSms("Código reenviado! Aguarde 2:00");

        var timerMinutes = 1;
        var timerSeconds = 59;
        var smsInterval = setInterval(() => {
          if(timerMinutes ===0 && timerSeconds === 0){
            setEnableBtnSms("");
            setLabelBtnSms("Reenviar código");
            clearInterval(smsInterval);
            return;
          }
          if(timerSeconds === 0){
            timerMinutes--;
            timerSeconds = 59;
          }
          var timerString = `${timerMinutes > 0 ? 0 + timerMinutes.toString() + ':': ''}${timerSeconds < 10 ? 0 + timerSeconds.toString() : timerSeconds.toString()}`;
          setLabelBtnSms(`Código reenviado! Aguarde ${timerString}`)
          timerSeconds--;
        }, 1000);
      });
  };

  const changeForm = (target: HTMLInputElement, erros: any) => {
    setValueForm({ ...getValues(), [target.name]: target.value });
    if(erros !== undefined){
      erros.message = '';
    }
    target.classList.remove("input-error");
    target.classList.add("input-enter");
  };

  const blurInputForm = (target: HTMLInputElement) => {
    target.classList.add("input-enter");
    if(!target.value || !target.validity.valid || 
      (target.name === 'password' && target.value.length < 6)){
      target.classList.remove("input-enter");
      target.classList.add("input-error");
    }else{
      target.classList.add("input-valid");
      target.classList.remove("input-enter");
      target.classList.remove("input-error");
    }
    setValueForm({ ...getValues(), [target.name]: target.value });
  };

  const focusInput = (target: HTMLInputElement) => {
    target.classList.add("input-enter");
  };

  return (
    <form className="align-center-form" onSubmit={handleSubmit(onSubmit)}>
      <h2><b>Verificação de Segurança</b></h2>
      <p>Para proteger sua conta, conclua a seguinte verificação.</p>

      <MaskedInput
        register={register("password", {
          onBlur: (e) => blurInputForm(e.target)
        })}
        error={errors.password?.message}
        name="password"
        type="text"
        label="Verificação por telefone"
        placeholder="******"
        onChange={(e) => changeForm(e.target, errors.password)}
        mask="999999" maskChar={null}
        onFocus={(e:any) => focusInput(e.target)}
      />
      
      <p>Digite o código de 6 digitos enviado para 319****4040.</p>
      <div className={`link-confirmacao-login ${enableBtnSms}`} onClick={onResendSms} >{labelBtnSms}</div>
      <Button
        type="submit"
        className="md buttonVer btn btn-enviar"
        disabled={!validationSchema.isValidSync(valueForm)}
      >
        Enviar
      </Button>
      <div className="link-confirmacao-login">A verificação de segurança não está disponível?</div>
    </form>
  );
};

export default VerificacaoLogin;
