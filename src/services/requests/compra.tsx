import http from "../index";

class CompraService {

  comprar(quantidade: number) {
    return http.post("/payments/checkout-session", {"quantity": quantidade}, {headers: {

      "Authorization": `Bearer ${localStorage.getItem("token")}`

    }});
  }

}

export default new CompraService();