import http from "../index";
import FormLoginData from "../../types/FormLogin.interface"
import FormNovaSenhaData from "../../types/FormNovaSenha.interface";
import FormEsqueceuSenhaData from "../../types/FormEsqueceuSenha.interface";

class LoginService {
  login(data: FormLoginData) {
    return http.post("/account/login", data);
  }

  confirmarLogin(data: any) {
    return http.post("/account/login-two-step", data);
  }

  confirmarEmail(data: any) {
    return http.post("/account/confirm-email", data);
  }

  logout(data: FormLoginData) {
    return http.post(`/logout`, data);
  }

  novaSenha(data: FormNovaSenhaData) {
    return http.post(`/novaSenha`, data);
  }

  resetSenha(data: any) {
    return http.post(`/account/reset-password`, data);
  }

  esqueceuSenha(data: FormEsqueceuSenhaData) {
    data.returnUrl = "nova-senha";
    return http.post(`/account/forgot-password`, data);
  }

  reenviarSms(email: any){
    return http.post(`/account/resend-two-factor-code`, {"email": email});
  }

  reenviarEmailConfirmar(email: any){
    return http.post(`/account/resend-email-confirmation`, {"email": email});
  }
}

export default new LoginService();