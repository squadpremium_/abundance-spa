import axios, { AxiosInstance } from "axios";

const axiosInstance: AxiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL_API,
  headers: {
    "Content-type": "application/json",
    "Authorization": `Bearer ${localStorage.getItem("token")}`
  },
 // withCredentials: false
});

export default axiosInstance;