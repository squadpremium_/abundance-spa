export default interface FormEsqueceuSenhaData {
    returnUrl: string;
    email: string;
    token: string;
    password: string;
    passwordConfirm: string;
}