export default interface FormCadastroData {
    name: string;
    email: string;
    nomeEmpresa: string;
    password: string;
    passwordConfirm: string;
    atualizarInfo: boolean | string;
    ondeOuviu: string;
    phoneNumber:string;
    isValidPhysicalPerson: boolean;
}