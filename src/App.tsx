import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { persistor } from "./store";
import { PersistGate } from "redux-persist/integration/react";
import axios from "../src/services/index";

import Routes from "./routes";
import AlertModal from "./components/AlertModal";
import Loader from "./components/Loader";

const App =() => {
  const dispatch = useDispatch();
  
  const [message, setMessage] = useState({
    opened: false, 
    message: '' ,
    type: 'warning' //error success
  })

  let axiosRequest: any;
  let axiosResponse: any;

  useEffect(() => {

    // dispatch({ type: "USER_LOGIN_SUCCESS",
    // payload: {
    //   user: null,
    // token: ""
    // }});

    if (!!axiosRequest || axiosRequest === 0) {
      axios.interceptors.request.eject(axiosRequest);
    }

    if (!!axiosResponse || axiosResponse === 0) {
      axios.interceptors.request.eject(axiosResponse);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    axiosRequest = axios.interceptors.request.use(async (config) =>{
      setMessage({message: '', opened: false, type: 'error'});
      dispatch({ type: "LOADING_ACTIVE"});
      return config;
      }, (error) => {
        dispatch({ type: "LOADING_INACTIVE"});
        return Promise.reject(error);
      });

      // eslint-disable-next-line react-hooks/exhaustive-deps
      axiosResponse = axios.interceptors.response.use((response) => {
        
        setMessage({message: '', opened: false, type: 'error'});
        
        if(response?.data?.statusMessage === `Succeeded` && response?.data?.authResponse){
          var user = {
            id: response.data.authResponse.userToken.id,
            name: response.data.authResponse.userToken.claims[0].value,
            email: response.data.authResponse.userToken.email,
            phoneNumber: response.data.authResponse.userToken.claims[1].value,
            shortName: ""
          };
    
          var arrayName = user.name.split(' ');
          user.shortName = arrayName[0].substr(0,1) + arrayName[arrayName.length-1].substr(0,1);
    
          //localStorage.setItem("token", response.data.authResponse.accessToken);
          dispatch({ type: "USER_LOGIN_SUCCESS",
          payload: {
            user,
            token: response.data.authResponse.accessToken
          }});
        }
        // else if(response?.data?.debuggingWithSwagger){//remover quando subir o projeto
        //   var json = JSON.stringify(response?.data?.debuggingWithSwagger);
        //   //console.log(json);
        //   localStorage.setItem("controls", json);
        //   //alert(json);
        // }
    
        dispatch({ type: "LOADING_INACTIVE"});
        return response;
      }
      ,(error) => {
        
        //if(!(error?.response?.data?.url)){
          let messageError = '';
          if(error.response?.data?.errors?.Messages){
            messageError = error.response.data.errors.Messages[0];
          }
          else if(error.response?.request?.response){
            messageError = error.response?.request?.response.split('errors":{"')[1].toString();
            messageError = messageError.substring(0, messageError.length-4);
          }
          else{
            messageError = error.message;
          }
          setMessage({message: messageError, opened: true, type: 'error'});
    
          dispatch({ type: "LOADING_INACTIVE"});
    
          if(error?.config?.url === '/account/confirm-email'){
            return error.response;
          }
          return Promise.reject(error);      
        //}    
        //return error?.response;
      }
    );
  }, [])

  return (

    
    <div>
      <Loader></Loader>
      <AlertModal type={message.type} smallText={true} opened={message.opened} onClose={() => setMessage({message: "", type: "error", opened: false})}>
         {message.message}
        </AlertModal>
      <PersistGate loading={true} persistor={persistor}>
        <Routes />
      </PersistGate>
    </div>
  );
}


export default App;

