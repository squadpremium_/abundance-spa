import React, { useState } from "react";
import CardIndiqueAbundance from "../Cards/CardIndiqueAbundance";
import CardNovaConta from "../Cards/CardNovaConta";
import LinkButton from "../LinkButton";
import Select from "../Select";
import "./style.scss";


export default function InfoJaTemConta() {

  return (
    <div className='container-info-create-account'>
      <label className="label-info label-info-1">Já possui uma conta?</label>
      <LinkButton className="sm btn gray-transparent" to="/login">Acesse sua conta</LinkButton>
    </div>
  );
}
