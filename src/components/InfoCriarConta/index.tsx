import React, { useState } from "react";
import CardIndiqueAbundance from "../Cards/CardIndiqueAbundance";
import CardNovaConta from "../Cards/CardNovaConta";
import LinkButton from "../LinkButton";
import Select from "../Select";
import "./style.scss";


export default function InfoCriarConta() {

  const [openMenu, setOpemMenu] = useState(false)

  const openMenuHandle = () => {
    setOpemMenu(!openMenu);
  };

  return (
    <div className='container-info-create-account'>
      <label className="label-info label-info-1">Não possui uma conta?</label>
      <div className="link-menu-cadastro" onClick={(e) => openMenuHandle()}>
        <div className="label-info">Abra já a sua!</div>
        <span className="material-icons icon-new-account">chevron_right</span>
      </div>
      {openMenu ? 
        <div className="menu-cadastro">
          <CardNovaConta></CardNovaConta>
        </div>
        :
        <div></div>
      }
      {/* CardIndiqueAbundance */}
      {/* <Select
        // register={register("ondeOuviu")}
        // error={errors.ondeOuviu?.message}
        name="ondeOuviu"
        // label="Aonde você ouviu falar da Abundance Brasil?"
        // onChange={(e) => changeForm(e.target)}

      >
        <option value="pf">Para você</option>
        <option value="pj">Para sua empresa</option>

      </Select> */}
      {/* <LinkButton className="sm btn gray-transparent" to="/cadastro">Crie sua conta</LinkButton> */}
    </div>
  );
}
