import React from "react";

interface PropsButton {
  id?: string;
  disabled?: boolean;
  className?: string;
  children?: any;
  type?: any;
  tabIndex?: number;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
}

export default function Button(props: PropsButton) {
  return (
    <button 
      tabIndex={props.tabIndex}
      type={props.type}
      id={props.id}
      className={`btn ${props.className}`}
      onClick={props.onClick}
      disabled={props.disabled}>
      {props.children}
    </button>
  );
}
