import "./style.scss";
import Button from "../Button";
import { useState } from "react";
import DrawerVA from "../SliderModalVA";
import FormConfirmacao from "../../containers/DrawerInformations/FormConfirmacao";
interface PropsCompra {
  quantidade: number;
}

const ValueCompra = ( props: PropsCompra ) => {
    const [openModal, setOpenModal] = useState(false);

    const formatNumber = (q:any) => {
      return q.toLocaleString('pt-BR', {
          style: 'currency',
          currency: 'BRL'
      })
     } 

    return(
        <div className="total-compra">
          <p className="subheading text-semibold text-dark-green">Total: <span className="text-purple">{formatNumber(props.quantidade * 100)}</span></p>
          <Button
          disabled={props.quantidade <1 ? true: false}
          onClick={() => setOpenModal(true)} className="button-comprar btn" >Comprar</Button>
          <DrawerVA opened={openModal} onCLose={() => setOpenModal(false)}>
            <FormConfirmacao quantidade={props.quantidade}/>
          </DrawerVA>
        </div>
    );
  }
  export default ValueCompra
  

