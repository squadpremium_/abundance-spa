import React from "react";
import { useHistory } from "react-router-dom";
import Button from "../../Button";
import "./style.scss";

export default function CardNovaConta() {
  const history = useHistory();

  const redirectTo = (tipo: string) => {
    if(tipo === "pf"){
      history.replace("/cadastro")
    } else {
      history.replace("/cadastro-pj")
    }

  };

  return (
    <div className="card-nova-conta">
      <div className="title-container">
        <p className="caption text-lightGray link-label" onClick={(e) => redirectTo("pf")}>Para você</p>
        <p className="caption text-lightGray link-label" onClick={(e) => redirectTo("pj")}>Para sua empresa</p>
      </div>
    </div>
  );
}
