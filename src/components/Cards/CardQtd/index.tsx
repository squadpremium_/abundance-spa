import React from "react";
import { useState } from "react";
import "./style.scss";
interface PropsQtd {
  onchange: any;
}

export default function CardQtd(props: PropsQtd) {
  const [counter, setCounter] = useState(0);
  const [disableBtn, setDisableBtn] = useState(false);

  const handleChangeCount = (qtd:number) =>{
    setCounter(qtd);
    props.onchange(qtd)
  }
  
  return (
    <div className="card-quantidade">
      <div className="title-qtd-01">
        <h5 className="heading-06 text-dark-green">Quantidade</h5>
        <p className="m-0 text-lightGray">1 AT = 1 Tonelada de CO2</p>
      </div>

      <div className="counter">
        <button
        className="btn-counter"
          disabled={counter === 0 || disableBtn}
          onClick={() => handleChangeCount(counter - 1)}
        >
            <i className="material-icons">minimize</i>
          
        </button>

        <p className="p-02">{counter}</p>

        <button
          className="material-icons btn-counter"
          disabled={disableBtn}
          onClick={() => handleChangeCount(counter + 1)}
        >
          add
        </button>
      </div>
    </div>
  );
}
