import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import AlertModal from "../components/AlertModal";
import Loader from "../components/Loader";

import Routes from "./index";

const App = () => {
  
  return (
    <Router>
      <Loader></Loader>
      {/* <AlertModal type={message.type} smallText={true} opened={message.opened} onClose={() => setMessage({message: "", type: "error", opened: false})}>
        {message.message}
      </AlertModal> */}
      <div>
        <Routes />
      </div>
    </Router>
  );
};

export default App;
